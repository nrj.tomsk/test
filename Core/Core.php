<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 19:53
 */

namespace Core;


/**
 * Class Core
 * @package system
 */
class Core
{
    /**
     * Core constructor.
     */
    public function __construct() {
        Request::setParams();
        $this->activeController();
    }

    protected function activeController(){
        $route = new Route();
        $currentController = $route->getCurrentRoute();
        $controllerName = '\\App\\Controllers\\' . $currentController['controller'].'Controller';
        $controller = new $controllerName;
        $actionNameName =  $currentController['method'] . 'Action';
        if(!method_exists($controller, $actionNameName)){
            throw new \Exception('Not fount action '.$currentController['method'], 404);
        }
        $controller->$actionNameName($currentController['argm']);
    }
}