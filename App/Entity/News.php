<?php

namespace App\Entity;


use App\Repository\ImageRepository;

class News
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string | null
     */
    public $text;

    /**
     * @var string
     */
    public $updatedAt;

    /**
     * @var int | null
     */
    public $imageId;

    /**
     * @var string
     */
    public $imageUrl;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


    /**
     * @return int | null
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setImageId(int $id = null)
    {
        $this->imageId = $id;
        return $this;
    }

    /**
     * @return $this
     */
    public function setImageUrl()
    {
        if($this->getImageId() == null) {
            return $this;
        }

        $imageRepository = new ImageRepository();
        $this->imageUrl = $imageRepository->getUrlById($this->getImageId());
        return $this;
    }
}