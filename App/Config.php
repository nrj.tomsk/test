<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database driver
     * @var string
     */
    const DB_DRIVER = 'mysql';

    /**
     * Database charset
     * @var string
     */
    const DB_CHARSET = 'utf8';

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'smpl';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;

    /**
     * Uploaded dir
     * @var string
     */
    const UPLOADED_DIR = 'uploads';
}
