<?php

namespace App\Repository;

use App\Repository\Exception\ImageRepositoryException;
use Core\Repository;
use PDO;

/**
 * Class Image
 * @package App\Models
 */
class ImageRepository extends Repository
{
    /** @var PDO */
    public $db;

    /**
     * Image constructor.
     */
    public function __construct()
    {
        $this->db = static::getDB();;
    }


    /**
     * @param string $url
     * @return int
     */
    public function addImageInDB(string $url): int
    {
        $stmt = $this->db->prepare('INSERT INTO image (url) VALUES (:url)');
        $stmt->bindParam(':url', $url);
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    /**
     * @param int $id
     * @return string
     */
    public function getUrlById(int $id): string
    {

        $stmt = $this->db->prepare('SELECT url FROM image WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return isset($result['url']) ? $result['url'] : '';
    }

    /**
     * @param int|null $id
     * @throws ImageRepositoryException
     */
    public function delete(int $id = null)
    {
        if($id == null) {
            return;
        }
        try {
            unlink(trim($this->getUrlById($id), '/'));
        } catch (\ErrorException $e) {
            throw new ImageRepositoryException('Not found file');
        }
        $stmt = $this->db->prepare('DELETE FROM News WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }


}