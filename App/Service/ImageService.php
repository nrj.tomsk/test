<?php

namespace App\Service;

use App\Config;

class ImageService
{
    /**
     * @param $file
     * @return string
     * @throws \ErrorException
     */
    public function uploadImage($file): string
    {
        $uploaddir = Config::UPLOADED_DIR;
        $uploadfile = $uploaddir .'/'. basename($file['name']);
        move_uploaded_file($file['tmp_name'], $uploadfile);

        return '/'. Config::UPLOADED_DIR . '/'. $file['name'];
    }
}